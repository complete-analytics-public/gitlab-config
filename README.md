# Gitlab-Config

Holds general configuration for re-use via an include in project's Gitlab yml files. Non-sensitive information only.

### Usage

1. Include relevant files in your projects .gitlab-ci.yml file:
    ```yml
    include:
        - https://gitlab.com/complete-analytics-public/gitlab-config/raw/master/base.gitlab.yml
        - https://gitlab.com/complete-analytics-public/gitlab-config/raw/master/build.gitlab.yml
        - https://gitlab.com/complete-analytics-public/gitlab-config/raw/master/deploy.gitlab.yml
    ```

2. To extend / override a job, name it the same and set properties:
    ```yml
    deploy-dev:
        environment:
            name: development
            url: https://app.example.com
    ```

3. To specify a different Docker root path or Dockerfile, in your .gitlab-ci.yml override one or more of the following:
    ```yml
    variables:
        BUILD_PATH: ./path/to/build/directory
        BUILD_FILE: Dockerfile.prod.build
        TEST_PATH: ./path/to/test/directory
        TEST_FILE: tests/Dockerfile
    ```
    - As shown above:
        - The build path represents the root inside a Dockerfile
        - The Dockerfile can have a different name and in this case is in the root of the build path
        - The Dockerfile can be in sub directory(s) e.g. `tests` of Docker build path
            - Note the absense of a preceding `/` or `./` in this case

4. Specify alternate tag name for latest build:
    ```yml
    build-special:
        extends: build-latest
        variables:
            IMAGE_TAG: special
    ```

5. Specify alternate tag name for latest build:
    ```yml
    build-latest:
        variables:
            IMAGE_TAG: local
    ```

6. Provide Docker build arguments:
    ```yml
    build-release:
        variables:
            BUILD_ARGS:
                --build-arg CONFIGURATION=staging
                --build-arg COMPRESS=true
    ```
    - These will be injected into the Dockerfile at build time
    - **Caution:** Quotes are un-reliable inside variable strings due to the Gitlab escaping mechanism
        - Example, this will fail:
            ```yml
            BUILD_ARGS: --build-arg EXTRA_BUILD_ARGS="--compress --package"
            ```


> Variables will be expanded as normal

> Lists / Arrays such as `script` will be overriden so repeat any items to retain them
